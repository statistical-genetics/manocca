# Gitlab repository for the MANOCCA project

Tool used to compute the covariance test on a set of predictors, presented in this paper : https://academic.oup.com/bib/article/25/4/bbae272/7690346 MANOCCA (Multivariate ANalysis of Conditional CovAriance) performs the covariance test : $Cov(Y_1,Y_2) \sim X + C$, as well as the multivariate version with more than two Y outcomes. In quick words, MANOCCA (Multivariate ANalysis of Conditional CovAriance) is a test of differences in correlations/covariance with regard to a predictor. The test is orthogonal to mean and variance effects, and will only grasp covariance effects.

One application of the MANOCCA test to detect co-abunding human gut microbiome communities can be found in this preprint : https://www.medrxiv.org/content/10.1101/2024.04.30.24306630v1.full

Note : If all outcomes are centered and scaled, then $Corr(Y_1, Y_2) = Cov(Y_1,Y_2)$ and MANOCCA can test for changes in correlation. The subtleties of covariance vs correlation are discussed in the main draft and supplements of the paper above. 

Author : Christophe BOETTO

Archived commit pinned for the article https://academic.oup.com/bib/article/25/4/bbae272/7690346 in case the tool evolves in the future : 6d76df07
To go to this specific commit do :
- git clone <manocca_https_link>
- cd manocca
- git reset --hard 6d76df07


# Contents
The current repository contains the Python and R versions of MANOCCA. The python is more detailed than the R version, but they both provide the same test. 
In the Python version you will find :
- MANOCCA - the multivariate test on covariance
- MANOVA - the multivariate test on mean
- Explainer - A tool to help explain the results from a MANOCCA test. You can for instance use it to check the power with regard to the number of PCs kept, of the main loadings of some PCs.
- the rest is mostly functionalities under development. 

# Python Installation
Start by creating a Python venv in the directory with the requirements.txt file. You can run the command :

```
python -m venv name_of_your_env
source name_of_your_env/bin/activate
pip install --upgrade pip
pip install -r requirements.txt
```

In windows you need to change the second line with :
```
name_of_your_env/Scripts/activate
```
You can then launch :
```
jupyter notebook
```
And check the notebook Example_manocca.ipynb where I compiled a couple of examples on how to use MANOCCA.

# Docker
In the Python repository you will find a Dockerfile, which can be build and ran interactively.
Follow these steps :
- Download MANOCCA repository, and go in the Python directory
- Launch you docker engine (ex : launch the docker application)
- Then run the following command in your terminal while in the the MANOCCA/python directory (or prompt in windows)
- docker build -t manocca_docker .
- docker run -it manocca_docker

This will allow you to run an interactive terminal with the right environment to make MANOCCA work. If you want to use your personal data, you can add a repository in MANOCCA/python/your_data_directory and the data will be copied in the docker build command and you can access it within the interactive docker. If your dataset is too big, consider looking at the docker documentation to access data from an interactiver container.
Remark : jupyter notebook might be tricky to run from a container, so use the Example_manocca_script.py to get started. 

# Getting started 
You can start by taking a look at some examples in the Python repository :
- Jupyter notebook Example_manocca.ipynb
- Python script : Example_manocca_script.py

Steps to launche the jupyter notebook :
- open Terminal or windows prompt
- go to the MANOCCA/python/ directory
- launch virtual environment like stated above
- write : jupyter notebook
- wait for your web navigator to open, or paste link in the terminal
- open : Example_manocca.ipynb