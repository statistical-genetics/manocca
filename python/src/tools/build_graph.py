import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

from tqdm import tqdm

from pyvis.network import Network #other option gravis
import plotly.graph_objects as go
import networkx as nx


def get_otu_tree(otu, col='OTU'):
    d_otu = df_tree[df_tree[col]==otu].to_dict('records')
    d_otu = d_otu[0]
    
    to_display = ''
    for key in d_otu.keys():
        to_display = to_display + key + ' : ' + d_otu[key] + '\n'
    return to_display

def get_tree(df_tree, col) :
    d_tree = {}
    for el in df_tree[col].unique():
        name = "\n".join(df_tree[df_tree[col]==el].apply(lambda x : x.name + " : " + x.values).values[0])
        d_tree[el] = name
    return d_tree


def build_graph(L_d_otu_pvals, name = 'test.html', tree = None, figsize = (800,800), show = True, notebook = True):
    """
    The function build_graph constructs a network based on the output contribution for a list of predictors. The network is based on the networkw package 
    and rendered using pyvis.
    Input :
        - L_d_otu_pvals : list of dictionaries : list for each predictor of the top contributions to plot in the network. 
                i.e. for 2 predictors and 4 features F1, F2, F3, F4 [{"F1|F2" : 0.3, "F1|F4" : 0.1}, {"F1|F3" : 0.8, "F2|F4" : 0.02}]
        - name : str - name to use to save the network before display. Mandatory for displaying the network.
        - tree : dictionary - dictionary with key being the node names (features) and values being the string to display when hovering
                i.e. for 4 features F1 and F2: {"F1" : "Feature 1, experimental factor", "F2" : "Order" : "order2 \n genus : genus2"}
        - figsize : tuple - Size of figure (height,width)
        - show : bool - display network, if False returns the network
        - notebook : bool - True when displaying in jupyter notebook, else set to False
    Output :
        - None if show is True, else returns net, G the constructed pyvis network and networkx graph

    """

    nodes = set()
    edges = set()

    cmap = plt.cm.rainbow
    
    n_pheno = len(L_d_otu_pvals)
    
    d_color_id = {}
    
    G=nx.Graph()
    
    for k, d_otu_pvals in enumerate(L_d_otu_pvals):
        color_id = k * int(cmap.N/(n_pheno))
        for otux in tqdm(d_otu_pvals):
            otux_split = otux.split('|')
            node_names = []
            for otu in otux_split: # loop on the two features
                tree_display = tree[otu]
                node_name = otu #+ "\n" + tree_display.split(' : ')[-1]
                node_names += [node_name]
#                 print(node_names)
                if node_name not in nodes : # create node if not already created
#                     print(nodes)
#                     print(node_name)
                    G.add_node(node_name, size = 10, title = tree_display, color = mcolors.rgb2hex(cmap(color_id))) #  '#F0F8FF')
                    d_color_id[node_name] = [color_id]
    #                     print(G[node_name])
                    nodes.add(node_name)
                else :
#                     print(G.nodes[node_name])
                    if color_id not in d_color_id[node_name]:
                        d_color_id[node_name]+=[color_id]
                        new_color_id = int(sum(d_color_id[node_name])/len(d_color_id[node_name]))
                        nx.set_node_attributes(G,{node_name : mcolors.rgb2hex(cmap(new_color_id)) },'color')
                    else :
                        pass
#                     print(node_name)
#                     G.nodes[node_name]['color'] = 'b'# mcolors.rgb2hex(cmap(color_id))
#                     if node_name not in d_color_id.keys():
#                         nx.set_node_attributes(G,{node_name : 'black'},'color')
#                     G[node_name][0]["color"] = 'y'


            # Create edge between nodes
    #         G.add_edge(otux_split[0],otux_split[1], value = d_otu_pvals[otux], title = d_otu_pvals[otux])
    #         edges.add((otux_split[0],otux_split[1]))

            ### if genus is added
            if (node_names[0],node_names[1]) not in edges :
                G.add_edge(node_names[0],node_names[1], value = d_otu_pvals[otux], title = d_otu_pvals[otux], color = 'grey')
                edges.add((node_names[0],node_names[1]))
    
    # Change node size
    d=dict(G.degree)
    d.update((x,10*np.sqrt(y)) for x, y in d.items())
    nx.set_node_attributes(G,d,'size')
    
    
    # add legend
    # Name_features = ["AGE", "SEX", 'BMI', 'TABAC']
    # color_1 = mcolors.rgb2hex(cmap(int(0*cmap.N/(n_pheno-1))))
    # color_2 = mcolors.rgb2hex(cmap(int(1*cmap.N/(n_pheno-1))))
    # G.add_node("AGE", size = 100, color = mcolors.rgb2hex(cmap(0*int(cmap.N/(n_pheno-1)))))
    # G.add_node("SEX", size = 100, color = mcolors.rgb2hex(cmap(1*int(cmap.N/(n_pheno-1)))))
    # G.add_node("BMI", size = 100, color = mcolors.rgb2hex(cmap(2*int(cmap.N/(n_pheno-1)))))
    # G.add_node("TABAC", size = 100, color = mcolors.rgb2hex(cmap(3*int(cmap.N/(n_pheno-1)))))
    # G.add_node("AGE x SEX", size = 100, color = mcolors.rgb2hex(cmap(int(((0+1)/2)*cmap.N/(n_pheno-1)))))
    # G.add_node("SEX x BMI", size = 100, color = mcolors.rgb2hex(cmap(int(((1+2)/2)*cmap.N/(n_pheno-1)))))
    # G.add_node("BMI x TABAC", size = 100, color = mcolors.rgb2hex(cmap(int(((2+3)/2)*cmap.N/(n_pheno-1)))))
    # G.add_node("AGE x TABAC", size = 100, color = mcolors.rgb2hex(cmap(int(((0+3)/2)*cmap.N/(n_pheno-1)))))


    ### DISPLAY ###
    net = Network(height = figsize[0], width = figsize[1], notebook = notebook)
#     net = Network("500px", "500px", notebook = True)
    net.show_buttons(filter_=['physics'])
    net.from_nx(G)
    if show :
        display(net.show(name))
    else :
        return net, G