import numpy as np
import pandas as pd


def shannon_diversity(d):
    if isinstance(d,pd.DataFrame):
        return (d/d.sum()*np.log(d/d.sum())).fillna(0).sum() #fillna for log of 0 values chich produce NaNs
    else :
        return np.nan_to_num(d/d.sum()*np.log(d/d.sum())).sum()



def simpson_diversity(d):
    return 1 - (d*(d-1)).sum()/(d.sum()*(d.sum()-1))