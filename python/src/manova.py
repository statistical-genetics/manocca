import numpy as np
import pandas as pd

# from sklearn.preprocessing import scale
from tools.preprocessing_tools import scale

from tqdm import tqdm

import tools.preprocessing_tools as pt
from tools.compute_manova import custom_manova

class MANOVA :
    def __init__(self, predictors, outputs, covariates=None, cols_outputs = None,
                 cols_predictors = None, cols_covariates = None, use_resid = True, return_fisher=False):
        ### Initializing 
        self.outputs = outputs
        self.cols_outputs = cols_outputs
        self.outputs, self.cols_outputs = pt._extract_cols(self.outputs, self.cols_outputs)
        self.outputs = scale(self.outputs)

        self.predictors = predictors
        self.cols_predictors = cols_predictors
        self.predictors, self.cols_predictors = pt._extract_cols(self.predictors, self.cols_predictors)
        self.predictors = scale(self.predictors)


        self.covariates = covariates
        self.cols_covariates = cols_covariates
        if covariates is not None :
            self.covariates, self.cols_covariates = pt._extract_cols(self.covariates, self.cols_covariates) 
            self.covariates = scale(self.covariates)

        self.use_resid = use_resid
        if use_resid and isinstance(self.covariates, np.ndarray):
            print("computing residuals")
            self.outputs = np.apply_along_axis(lambda x : pt.adjust_covariates(x,self.covariates), axis = 0, arr = self.outputs) 
            # print("adjusting predictor")
            # self.predictors = np.apply_along_axis(lambda x : pt.adjust_covariates(x,covariates), axis = 0, arr = self.predictors)

        self.return_fisher = return_fisher

        # For results
        self.p = None
        self.fisher = None



    def test(self, var = None, **kwargs):
        if isinstance(var, type(None)) :
            var = self.predictors
        else :
            var = scale(var)

        nb_tests = var.shape[1]
        p = np.empty((nb_tests,1))
        if self.return_fisher:
            fisher = np.empty((nb_tests,1))

        for i_pred in range(nb_tests):
            nan_mask = np.isnan(var[:,i_pred])
            if ((self.covariates is not None) and (self.use_resid == False)) :
                # print("With covariates")
                if self.return_fisher :
                    p[i_pred], fisher[i_pred] = custom_manova(self.outputs[~nan_mask,:], var[~nan_mask,i_pred].reshape(-1,1), self.covariates[~nan_mask,:], return_fisher=self.return_fisher)
                else :
                    p[i_pred] = custom_manova(self.outputs[~nan_mask,:], var[~nan_mask,i_pred].reshape(-1,1), self.covariates[~nan_mask,:])
            else :
                # print("Without covariates")
                if self.return_fisher :
                    p[i_pred], fisher[i_pred] = custom_manova(self.outputs[~nan_mask,:], var[~nan_mask,i_pred].reshape(-1,1), return_fisher=self.return_fisher)
                else :
                    p[i_pred] = custom_manova(self.outputs[~nan_mask,:], var[~nan_mask,i_pred].reshape(-1,1))
        self.p = p
        if self.return_fisher:
            self.fisher = fisher




# ### TESTS ###

# a = pd.DataFrame(np.random.randint(0,10,(10,5)), columns = ["cols_" + str(i) for i in range(5)])
# b = pd.DataFrame(np.random.randint(0,10,(10,5)), columns = ["cols_pred_" + str(i) for i in range(5)])
# b = b.iloc[:,0]
# print(a)

# manova = MANOVA(a,b, cols_predictors = ['a'])
# manova.test()
# print(manova.p)