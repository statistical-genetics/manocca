import pandas as pd
import numpy as np

from manocca import MANOCCA
from manova import MANOVA
from univariate import UNIVARIATE

from tools.h_clustering import cluster_corr
#from tools.build_graph import get_tree, build_graph

import plotly.express as px
from plotly.subplots import make_subplots
import plotly.graph_objects as go
# import matplotlib.pyplot as plt

from tqdm import tqdm

class Explainer :
    """ Explainer for the MANOCCA model

    In order to give more insights into the test, the Explainer class provides a set of method to help understand the way MANOCCA performed. 

    Parameters
    ----------
    model : MANOCCA class
        An initialized MANOCCA instance. Can be retrieved from the Simulation class using : simu.get_model(0), where MANOCCA is the first model from L_methods

    
    Attributes
    ----------
    model : MANOCCA instance
    covMat : numpy array, covariance matrix for all outputs.
    df_loadings : pandas DataFrame, dataframe with the PCA loadings.

    """




    def __init__(self, model):
        self.model = model

        self.covMat = None
        self.cols_covMat = None

        self.df_loadings = None


    def _get_loadings(self):
        cols_outputs = np.array(self.model.cols_outputs.copy())

        # Retrieving PCA loadings
        if isinstance(self.model.pca, type(None)): # if we have a PCA we load the loadings
            print('No PCA found, inputing identity matrix as loadings')
            loadings = np.identity(cols_outputs.shape[0])
            self.df_loadings = pd.DataFrame(np.abs(loadings), columns = cols_outputs)
        else :
            pca = self.model.pca
            loadings = pca.components_
            

            cols_prod = []
            for i in range(len(cols_outputs)):
                for j in range(i+1,len(cols_outputs)):
                    cols_prod += [str(cols_outputs[i]) + '|'+ str(cols_outputs[j])]
            self.df_loadings = pd.DataFrame(np.abs(loadings), columns = cols_prod)
            # self.df_loadings = pd.DataFrame(loadings, columns = cols_prod)


    def power_pc_kept(self, pred, grid  = None, max_n_comp = None):
        """  Generates the p_values with different number of PC components kept.


        Parameters
        ----------
        pred : int or str
            Index or name of the specific predictor to test.
        grid : list
            Specify all the number of components thresholds you want to test, otherwise will by default put list(range(1, max_n_comp))
        max_n_comp : int
            Total number of cores allocated for the task. Equals to n_jobs in all cases, except when n_jobs=-1 where its value is cpu_count() from the joblib library.

        Returns
        -------
        (L_p, grid) : tuple
            With L_p the list of p_values for all thresholds and grid with all the thresholds tested
        """

        i_pred = self.model.cols_predictors.index(pred)
        var = self.model.predictors[:,i_pred].reshape(-1,1)

        if max_n_comp is None :
            max_n_comp = self.model.n_comp

        if grid is None : 
            grid = list(range(1,max_n_comp))

        L_p = []
        for el in grid :
            self.model.test(var = var, n_comp = el)
            L_p += [self.model.p[0]]
        
        L_p = np.array(L_p)

        return L_p, grid


    def significant_covariance(self, pc_num, max_loadings, plot = True):
        """  Displays side by side the covariance matrix and the corresponding top loadings for a given PC.


        Parameters
        ----------
        pc_num : int
            Index of the PC to use, !!! pc_num starts at 0 !!! 
        max_loadings : int
            Max number of loadings to display.
        plot : bool
            Put True if you want the plot. 

        Returns
        -------
        mat : np.ndarray (K_outcomes, K_outcomes)
            returns the matrix with only the top max_loadings covariances
        """

        cols_outputs = np.array(self.model.cols_outputs.copy())
        

        if isinstance(self.df_loadings, type(None)):
            self._get_loadings()

        tmp = self.df_loadings.iloc[pc_num,:]
        tmp.sort_values(ascending = False, inplace = True) # loadings are already taking in absolute value in _get_loadings()   , key = pd.Series.abs)

        top_loadings = tmp.copy()


        # We generate covariance matrix if not already computed
        if isinstance(self.covMat, type(None)):
            covMat = np.cov(self.model.outputs.T)
            covMat, idx = cluster_corr(covMat)
            cols_covMat = cols_outputs[idx]
            self.covMat = covMat
            self.cols_covMat = cols_covMat
        else :
            covMat = self.covMat
            cols_covMat = self.cols_covMat



        # mat = np.array([[np.nan]*covMat.shape[0]]*covMat.shape[1])
        mat = np.empty((covMat.shape[0],covMat.shape[1]))
        mat.fill(np.nan) # seemed to be the quickest to create a Nxk NaN matrix

        for k in range(max_loadings):
            el_ = top_loadings.index[k].split("|")

            cols_covMat = cols_covMat.astype(str)
            i = np.where(cols_covMat == str(el_[0]))[0][0]
            j = np.where(cols_covMat == str(el_[1]))[0][0]

            mat[i,j] = covMat[i,j]
            mat[j,i] = covMat[j,i]


        mat = pd.DataFrame(mat, columns = cols_covMat, index = cols_covMat)
        covMat = pd.DataFrame(covMat, columns = cols_covMat, index = cols_covMat)

        # display(px.imshow(covMat))

        fig = make_subplots(rows=1, cols=2)
        fig.update_layout(
            coloraxis=dict(colorscale='thermal', cmin = -1, cmax = 1)) #coloraxis_autocolorscale=False)

        fig.add_trace(
            px.imshow(mat, zmin = -1, zmax = 1).data[0],
            row=1, col=1
        )

        fig.add_trace(
            px.imshow(covMat, zmin = -1, zmax = 1).data[0],  #, color_continuous_scale = 'Darkmint'
            row=1, col=2
        )
        xmin, xmax = 0,covMat.shape[0]-1  # !!!! to change 
        ymin, ymax = covMat.shape[0]-1, 0 # !!!! to change 
        fig.update_xaxes(range=[xmin, xmax],autorange=False)
        fig.update_yaxes(range=[ymin, ymax], autorange=False)
        if plot :
            fig.show()
        
        return mat




    def univariate_cov(self, var, n_comp, return_beta = False):
        """  Runs an univariate screening for all covariance outcomes or PC if use_pca = True.


        Parameters
        ----------
        var : int or str
            Index or name of the specific predictor to test.
        n_comp : int
            Number of components to test, if use_pca = False, then put int(K_outcomes*(K_outcomes-1)/2). 
        max_n_comp : int
            Total number of cores allocated for the task. Equals to n_jobs in all cases, except when n_jobs=-1 where its value is cpu_count() from the joblib library.
        return_beta : bool
            Put True if you want the univariate coefficients and not the p_values.

        Returns
        -------
        univ.p : list of all univariate p_values
            With L_p the list of p_values for all thresholds and grid with all the thresholds tested
        """
        m = self.model

        i_var = m.cols_predictors.index(var)
        univ = UNIVARIATE(m.prodV_red[:,:n_comp], m.predictors[:,i_var].reshape(-1,1), m.covariates, cols_outputs = ['PC_' + str(i) for i in range(n_comp)], cols_predictors = ['var'])
        univ.test()

        if return_beta :
            return univ.p, univ.beta
        else :
            return univ.p


    def feature_importances(self, var, n_comp, threshold = 'n_comp', return_raw_contrib = False, square = True) :
        """
        Input :
            - var (int or str) - the index or name of the variable to analyze
            - n_comp (int) - number of components to keep in the model. If no pca was applied, then input the actual number of columns in your original dataset. It is highly recommended to use 'n_comp' than other parameters
                             Other options : - 'Bonferroni' : keeps only significant PC above bonferroni threshold
                                             - 'top_5' : keeps top 5 PCs. Works with any number: top_3, top_20, top_16, ...
                                             - 'all' : keeps all PCs, should be used if no PCA was applied
            - return_raw_contrib (bool) - returns the dataframe before computation of features with the p_values, chi2, singular values, ...
            - square (bool) - square or not loadings, highly recommended to use 'True' as the sum of mixed directions of effects is unclear.
        Ouput :
            - res (Serie) - returns a pd.Serie with pairs of features (feat1|feat2) as indexes and feature weight in the model as value. By default it is sorted in descending order. 


        """
        m = self.model
        i_var = m.cols_predictors.index(var)
        N_samples = np.sum(~np.isnan(m.predictors[:,i_var])) #retrieve sample (non-NaN values) size for the considered variable
        if isinstance(self.df_loadings, type(None)):
            self._get_loadings()

        p, beta = self.univariate_cov(var, n_comp, plot = False, return_beta = True)
        p = np.array(p).reshape(-1,1) #flip as a vertical vector
        df_p_beta = pd.DataFrame(p, columns = ['p'])

        df_p_beta['beta'] = beta
        df_p_beta['chi2'] = N_samples*beta**2


        if threshold == 'Bonferroni':
            df_p_beta = df_p_beta[df_p_beta<0.05/df_p_beta.shape[0]]


        elif 'top_' in threshold:
            df_p_beta.sort_values('p', inplace = True)
            nb_keep = int(threshold.split('_')[-1]) # retrieve number of pc to keep
            df_p_beta = df_p_beta.iloc[:nb_keep,:]

        elif threshold == 'n_comp' :
            df_p_beta = df_p_beta.iloc[:n_comp]


        elif threshold == 'all' :
            pass

        else :
            print('Threshold type not found')

        pc_to_look = list(df_p_beta.index)

        df_pc = self.df_loadings.loc[pc_to_look,:]

        # res = df_p_beta['chi2'].values.reshape(-1,1)*df_pc
        res = pd.merge(df_pc, df_p_beta, left_index = True, right_index = True ) 
        # display(res)

        if return_raw_contrib == True :
            res['singular_values_'] = self.model.pca.singular_values_.reshape(-1,1)
            return res
        else :
            df_loadings = res.iloc[:,:-3] #-3 because we added p, beta and chi2 at the end

            # ### TEST ###
            # df_loadings[df_loadings<0] = 0 # TEST get rid of negative loadings
            # need to test here if pca exists
            # if not isinstance(self.model.pca, type(None)):
            #     df_loadings = self.model.pca.singular_values_[list(df_loadings.index)].reshape(-1,1) * df_loadings # we get the singular values of each considered PCs
            ### END TEST ###

            if square == True :
                df_loadings = df_loadings*df_loadings

            # display(df_loadings)

            df_prod_contrib = res["chi2"].values.reshape(-1,1)*df_loadings
            return df_prod_contrib.sum().sort_values(ascending = False)#.to_dict()


    # def get_graph(self, L_d_otu_pvals, name = 'test.html', tree = None, figsize = (800,800), show = True, notebook = True):

    #     # Plot graph
    #     build_graph(L_d_otu_pvals, name = name, tree = tree, figsize = figsize, show = show, notebook = notebook)

    def split_contribution(self, prod_contrib, sep = "|"):
        df_contrib_otu = pd.Series(index = self.model.cols_outputs) 
        for otu in tqdm(df_contrib_otu.index) :
            df_contrib_otu[otu] = prod_contrib[prod_contrib.index.str.contains("^"+otu+"\||\|"+otu+"$", regex = True)].sum()
        return df_contrib_otu

