import numpy as np
import pandas as pd

# from sklearn.preprocessing import scale
from tools.preprocessing_tools import scale
from tools.diversity_metrics import shannon_diversity, simpson_diversity
from tools.compute_manova import linear_regression, ttest

from tqdm import tqdm



import tools.preprocessing_tools as pt
from tools.compute_manova import custom_manova

class ALPHA_DIVERSITY :
    def __init__(self, predictors, outputs, covariates=None, cols_outputs = None,
                 cols_predictors = None, cols_covariates = None, metric = "Simpson", use_resid = True):
        ### Initializing 

        self.metric = metric

        self.outputs = outputs
        self.cols_outputs = cols_outputs
        self.outputs, self.cols_outputs = pt._extract_cols(self.outputs, self.cols_outputs)
        self._apply_metric()
        self.outputs = scale(self.outputs)


        self.predictors = predictors
        self.cols_predictors = cols_predictors
        self.predictors, self.cols_predictors = pt._extract_cols(self.predictors, self.cols_predictors)
        self.predictors = scale(self.predictors)



        self.covariates = covariates
        self.cols_covariates = cols_covariates
        if covariates is not None :
            self.covariates, self.cols_covariates = pt._extract_cols(self.covariates, self.cols_covariates) 
            self.covariates = scale(self.covariates)

        self.use_resid = use_resid
        if use_resid and isinstance(self.covariates, np.ndarray):
            print("computing residuals")
            self.outputs = np.apply_along_axis(lambda x : pt.adjust_covariates(x,covariates), axis = 0, arr = self.outputs) 


    def _apply_metric(self):      
        if self.metric == 'Simpson':
            self.outputs = np.apply_along_axis(lambda x : simpson_diversity(x), axis = 1, arr = self.outputs).reshape(-1,1)
        elif self.metric == 'Shannon':
            self.outputs = np.apply_along_axis(lambda x : shannon_diversity(x), axis = 1, arr = self.outputs).reshape(-1,1)
        else :
            print("Unknown metric")

    def test(self, var = None, **kwargs):
        if isinstance(var, type(None)) :
            var = self.predictors
        else :
            var = scale(var).reshape(-1,1)

        nb_tests = var.shape[1] #self.predictors.shape[1]
        p = np.empty((nb_tests,1))
        for i_pred in range(nb_tests):
            nan_mask = np.isnan(var[:,i_pred])
            if ((self.covariates is not None) and (self.use_resid == False)) :
                # print("With covariates")
                beta = linear_regression(self.outputs[~nan_mask], var[~nan_mask,i_pred].reshape(-1,1), self.covariates[~nan_mask,:])
                p[i_pred] = ttest(beta, var[~nan_mask, i_pred], self.outputs[~nan_mask])
            else :
                # print("Without covariates")
                beta = linear_regression(self.outputs[~nan_mask,:], var[~nan_mask,i_pred].reshape(-1,1))
                p[i_pred] = ttest(beta, var[~nan_mask, i_pred].reshape(-1,1), self.outputs[~nan_mask])
        self.p = p



"""
        nan_mask = np.isnan(self.predictors).flatten()
        if ((self.covariates is not None) and (self.use_resid == False)) :
            print(self.outputs.shape)
            beta = linear_regression(self.outputs[~nan_mask], self.predictors[~nan_mask], self.covariates[~nan_mask])
        else :
            beta = linear_regression(self.outputs[~nan_mask], self.predictors[~nan_mask])
        self.beta = beta
        res = ttest(beta, self.predictors[~nan_mask], self.outputs[~nan_mask])
        self.p = res
"""

# ### TESTS ###

# a = pd.DataFrame(np.random.randint(0,10,(10,5)), columns = ["cols_" + str(i) for i in range(5)])
# b = pd.DataFrame(np.random.randint(0,10,(10,5)), columns = ["cols_pred_" + str(i) for i in range(5)])
# b = b.iloc[:,0]
# print(a)

# manova = MANOVA(a,b, cols_predictors = ['a'])
# manova.test()
# print(manova.p)