#!/usr/bin/env python
# coding: utf-8

# # MANOCCA example notebook
# 
# This notebook gives on overview on how to use MANOCCA. It is divided in 3 parts : 
# - Example of MANOCCA on data simulated without any signal (Null Hypothesis)
# - Example of MANOCCA without PCA applied on a dataset where N1 data were derived using a corMat1, and N2 using a modified correlation matrix corMat2
# - Example of MANOCCA with PCA on the same data
# 
# On the examples with changes in the correlation structure, examples using the Explainer tool or displayed.

# In[3]:


import numpy as np
import pandas as pd


# In[2]:


get_ipython().run_line_magic('load_ext', 'autoreload')
get_ipython().run_line_magic('autoreload', '2')


# In[5]:


import sys
sys.path.insert(0, 'src/') # path from your venv to the src
sys.path.insert(0, 'src/tools') # same but to src\\tools


# In[6]:


# import of my scaling function
from tools.preprocessing_tools import scale


# In[ ]:





# # First example : Null hypothesis
# In this scenario, the test runs on data without any signal

# In[7]:


#loading tools
from manocca import MANOCCA


# In[8]:


# Set up
N_samples = 1000
K_outputs = 10 # needs to be at least 2 to compute at least one covariance
K_predictors = 1


# In[14]:


outputs = np.random.normal(0,1,(N_samples,K_outputs))
predictors = np.random.binomial(1,0.4,(N_samples,K_predictors))


# In[12]:


# Compute the model : Y ~ X + C 
manocca = MANOCCA(predictors, # pd.DataFrame or np.ndarray : the matrix or vector X in the model, the predictor of covariance.
                  outputs, # pd.DataFrame or np.ndarray : the matrix Y in the model, in other words the covariance to study.
                  covariates = None, # pd.DataFrame or np.ndarray : the C in the model, the covariates to adjust.
                  cols_outputs = None, # specify column names of outputs, by default they are the column names of the pandas.DataFrame
                  cols_predictors = None, # specify column names of predictor, by default they are the column names of the pandas.DataFrame
                  cols_covariates = None, # specify column names of covariates, by default they are the column names of the pandas.DataFrame
                  prodV_red = None, # you can input a previous computed prodV_red (can be retrieved from manocca.prodV_red) to avoid unnecessary computation 
                  n_comp = None, # number of principal components to use, by default it will be min(N_samples, K_outputs*(K_outputs - 1)/2)
                  prod_to_keep = None, # usually None, unless you want to restrain the analysis to a specific list of products, ex : ['var1|var2','var1|var5',...] 
                  use_resid = True, # use residuals to adjust the covariates
                  use_pca = True, # use pca to reduce the dimension of the product matrix
                  n_jobs = 1 # number of jobs to use in parallel processing 
                 )


# In[15]:


manocca.test()

manocca.p


# # Simulation set up

# In[45]:


#loading tools
from simulation import Simu


# In[62]:


# Set up
N_samples = 1000
K_outputs = 10 # needs to be at least 2 to compute at least one covariance
K_predictors = 1


# In[64]:


real_cormat = np.array([[ 1.        ,  0.48280721,  0.00694387,  0.01185877, -0.07160852],
       [ 0.48280721,  1.        , -0.31682176, -0.32171591, -0.38747333],
       [ 0.00694387, -0.31682176,  1.        ,  0.9997097 ,  0.95207874],
       [ 0.01185877, -0.32171591,  0.9997097 ,  1.        ,  0.94824819],
       [-0.07160852, -0.38747333,  0.95207874,  0.94824819,  1.        ]])


# In[90]:


noise_cormat = np.corrcoef(np.random.normal(0,1,(100,K_outputs)).T)


# In[92]:


corMat1 = noise_cormat.copy()


corMat2 = noise_cormat.copy()
n_changes = real_cormat.shape[1]
corMat2[:n_changes, :n_changes] = real_cormat


# In[87]:


from plotly import subplots
import plotly.express as px


# In[95]:


px.imshow(corMat1, zmin = -1, zmax = 1)


# In[96]:


px.imshow(corMat2, zmin = -1, zmax = 1)


# In[116]:


# Get data
N1 = 600
N2 = 400

Y1 = np.random.multivariate_normal(np.zeros((K_outputs)), corMat1, N1)
Y2 = np.random.multivariate_normal(np.zeros((K_outputs)), corMat2, N2)

outputs = np.vstack([Y1,Y2])
predictors = scale(np.array([0]*N1 + [1]*N2)).reshape(-1,1)


# In[120]:


# Compute the model : Y ~ X + C 
manocca = MANOCCA(predictors, # pd.DataFrame or np.ndarray : the matrix or vector X in the model, the predictor of covariance.
                  outputs, # pd.DataFrame or np.ndarray : the matrix Y in the model, in other words the covariance to study.
                  covariates = None, # pd.DataFrame or np.ndarray : the C in the model, the covariates to adjust.
                  cols_outputs = None, # specify column names of outputs, by default they are the column names of the pandas.DataFrame
                  cols_predictors = None, # specify column names of predictor, by default they are the column names of the pandas.DataFrame
                  cols_covariates = None, # specify column names of covariates, by default they are the column names of the pandas.DataFrame
                  prodV_red = None, # you can input a previous computed prodV_red (can be retrieved from manocca.prodV_red) to avoid unnecessary computation 
                  n_comp = None, # number of principal components to use, by default it will be min(N_samples, K_outputs*(K_outputs - 1)/2)
                  use_pca = False,
                  use_resid = True, # use residuals to adjust the covariates
                  n_jobs = 1 # number of jobs to use in parallel processing 
                 )


# In[121]:


# Test
manocca.test()
manocca.p


# In[ ]:





# In[ ]:





# ## Explainer

# In[126]:


from explainer import Explainer

import matplotlib.pyplot as plt


# In[127]:


exp = Explainer(manocca)


# In[128]:


L_p, grid = exp.power_pc_kept(0, # index or column name of predictor to use 
                              grid = None, # grid of principal components to use
                              max_n_comp = None, # maximum number of principal components to use
                              plot = False  # plot the distribution of p_values 
                 ) # returns the list of p_value and grid of PC used (in order to plot easily)


# In[129]:


plt.scatter(grid, L_p)
plt.title("P_value in function of the number of principal components kept")
plt.show()


# In[130]:


help(exp.univariate_cov)


# In[131]:


L_p = exp.univariate_cov(0, # index or column name of predictor to use
                         n_comp = 10, # number of principal components to keep. Raw variables are used if no pca
                         plot = False # plot results
                        )# returns : list of p_value for each top n_comp principal component


# In[ ]:


9x


# In[145]:


top_features = exp.feature_importances(0, # variable to analyse
                                       45, # number of components to keep in the model, here no pca so 9*10/2
                                       threshold = "all" # recommmended to put n_comp for more information see function in code
                                      )

top_features


# In[ ]:





# In[136]:


### Showcasing explainer when PCA is used


# In[139]:


manocca_pca = MANOCCA(predictors, outputs, n_comp = 20, use_pca = True, use_resid = True, n_jobs = 1)
manocca_pca.test()
manocca_pca.p


# In[142]:


# loading new explainer
exp = Explainer(manocca_pca)


# In[144]:


top_features = exp.feature_importances(0, # variable to analyse
                                       10, # number of components to keep in the model
                                       threshold = "n_comp" # recommmended to put n_comp for more information see function in code
                                      )

top_features


# In[143]:


### Slightly depreciated function
# This function was developed before feature_importances(), and had for objective to detail differences in loadings for 
# significant PCs.
# Now feature_importances() retrieves the overall top important products 
exp.significant_covariance(0, # which pc to analyse, you can use univariate_cov to get significant principal components /!\ starts at 0
                           10 # number of loadings to display
                          )


# In[ ]:




